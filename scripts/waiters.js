// OPS WITH WAITERS      <!--Group wtr-ops-1: Edit and Remove; wtr-ops-2: Save, Confirm, Cancel -->

$(document).ready(function () {
  $('.wtr-ops-2').hide();
  $('.wtr-ops-1').css('visibility','hidden');
  $('.wtr-ops-1,.wtr-ops-2').css('margin','0');
});

$('#addWaiter').click(function () {
  $(this).prop('disabled',true);
  var rowCount = $('table tr').length;
  var markup = '<tr><td>' + rowCount + '</td><td><input type="text" class="form-control selected form-name" placeholder="Name"></td><td><input type="text" class="form-control selected form-login" placeholder="Login"></td><td><input type="password" class="form-control selected form-pw" placeholder="Password"></td><td><button class="btn btn-primary btn-block wtr-ops-2 wtr-save">Save</button><button class="btn btn-success btn-block wtr-ops-2 wtr-conf">Confirm</button><button class="btn btn-warning btn-block wtr-ops-1 wtr-edit">Edit</button></td><td><button class="btn btn-danger btn-block wtr-ops-2 wtr-cancel">Cancel</button><button class="btn btn-danger btn-block wtr-ops-1 wtr-del">Remove</button></td></tr>';
  // $('table tbody').append(markup);
  $(markup).appendTo('table tbody').addClass('waiter-line').find('*').addClass('selected').children('.wtr-conf,.wtr-ops-1').hide().css('margin','0');



  $('.wtr-save').click(function(){
    var name = $('.form-name').val();
    var login = $('.form-login').val();
    var pw = $(':password').val();
    $('.form-name').replaceWith('<p class="wtrName">' + name + '</p>');
    $('.form-login').replaceWith('<p class="wtrLogin">' + login + '</p>');
    $('.form-pw').replaceWith('<p class="wtrPassword">' + pw + '</p>');
    $('.wtr-save.selected').hide();
    $('.wtr-cancel.selected').hide();
    $('.wtr-ops-1.selected').show();
    $('#addWaiter').prop('disabled',false);
  });

  $('.waiter-line').on({
    mouseenter: function(){
      $(this).find('.wtr-ops-1').css('visibility','visible');
      $(this).find('*').addClass('selected');
  }, mouseleave: function(){
    $(this).find('.wtr-ops-1').css('visibility','hidden');
    $(this).find('*').removeClass('selected');
  }
  });

});

// $('.waiter-line').hover(
//   function () {
//     $(this).find('.wtr-ops-1').css('visibility','visible');
//     $(this).find('*').addClass('selected');
//   },
//   function () {
//     $(this).find('.wtr-ops-1').css('visibility','hidden');
//     $(this).find('*').removeClass('selected');
//   }
// );

$('.waiter-line').on({
  mouseenter: function(){
    $(this).find('.wtr-ops-1').css('visibility','visible');
    $(this).find('*').addClass('selected');
}, mouseleave: function(){
  $(this).find('.wtr-ops-1').css('visibility','hidden');
  $(this).find('*').removeClass('selected');
}
});

$('.wtr-edit').click(function(){
  $('.wtr-ops-1.selected').hide();
  $(this).siblings('.wtr-save').show();
  $('.wtr-cancel.selected').show();
  $('#addWaiter').prop('disabled',true);
  var name = $('.wtrName.selected').text();
  var login = $('.wtrLogin.selected').text();
  var pw = $('.wtrPassword.selected').text();

  $('.wtrName.selected').replaceWith('<input type="text" class="form-control selected form-name" placeholder="Name" value=' + name + '>');
  $('.wtrLogin.selected').replaceWith('<input type="text" class="form-control selected form-login" placeholder="Login" value=' + login + '>');
  $('.wtrPassword.selected').replaceWith('<input type="password" class="form-control selected form-pw" placeholder="Password" value=' + pw + '>');
});

$('.wtr-del').click(function(){
  $('.wtr-ops-1.selected').hide();
  $('.wtr-conf.selected,.wtr-cancel.selected').show();

  $('.wtr-conf').click(function(){
    $(this).parents('tr').remove();
  });
});

$('.wtr-save').click(function(){
  var name = $('.form-name').val();
  var login = $('.form-login').val();
  var pw = $(':password').val();
  $('.form-name').replaceWith('<p class="wtrName">' + name + '</p>');
  $('.form-login').replaceWith('<p class="wtrLogin">' + login + '</p>');
  $('.form-pw').replaceWith('<p class="wtrPassword">' + pw + '</p>');
  $('#addWaiter').prop('disabled',false);
  $('.wtr-save.selected').hide();
  $('.wtr-cancel.selected').hide();
  $('.wtr-ops-1.selected').show();
});

$('.wtr-cancel').click(function(){

  $('.wtr-save.selected').hide();
  $('.wtr-cancel.selected').hide();
  $('.wtr-ops-1.selected').show();
  $('#addWaiter').prop('disabled',false);
});




//$('#addWaiter').addClass('btn-danger');
