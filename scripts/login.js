var baseURL = 'https://best-coffee-app.herokuapp.com';

$('.sign-in').click(function(){
  $('.sign-in').children().addClass('active');
  $('.sign-up').children().removeClass('active');
  $('.login-form').show();
  $('.register-form').hide();
});

$('.sign-up').click(function(){
  $('.sign-in').children().removeClass('active');
  $('.sign-up').children().addClass('active');
  $('.login-form').hide();
  $('.register-form').show();
});

$('.btn-reg').click(function(){
  var name = $('#newAdminName').val();
  var login = $('#newAdminLogin').val();
  var pw = $('#newAdminPw').val();

  var form = {
    "name": name,
    "login": login,
    "password": pw
  };
  $.ajax({
    url: baseURL + '/admin',
    dataType: 'json',
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(form),
    success: function(response){
      alert("Data loaded: " + response.answer);
    },
    error: function(){
      // alert("Error");
      $('<small id="newLoginHelp" class="form-text text-muted">This login already exists.</small>').appendTo('#newAdminLogin');
    }
  });
});



$('.btn-log').click(function(){

})
